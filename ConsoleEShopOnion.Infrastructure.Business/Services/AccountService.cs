﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Infrastructure.Business.Services
{
    public class AccountService : IAccountService
    {
        public IUnitOfWork Db { get; set; }

        public AccountService(IUnitOfWork db)
        {
            this.Db = db;
        }
        public IEnumerable<Account> GetAllAccounts()
        {
            return Db.Accounts.GetAll();
        }
        
        public Account GetAccountByLogin(string login)
        {
            Account account = Db.Accounts.Find(i => i.Login == login);
            if (account == null)
            {
                throw new ArgumentException($"There is no good with name '{login}'!");
            }
            return account;
        }
        public void EditPersonalInfo(PersonInfo personInfo, string value, int selectedField)
        {
            switch (selectedField)
            {
                case 1: personInfo.Name = value;
                    break;
                case 2: personInfo.Surname = value;
                    break;
                case 3: personInfo.Phone = value;
                    break;
                case 4: personInfo.Email = value;
                    break;
                default:
                    throw new ArgumentException("There is no such field!");
            }
        }
        public Account RegisterAccount(string login, string password)
        {
            if(Db.Accounts.Contains(i => i.Login == login))
            {
                throw new ArgumentException($"Account with login '{login}' already exists!");
            }
            if(password.Length < 8)
            {
                throw new ArgumentException("Password length should be more or equal 8!");
            }
            Account account = new Account(login, password);
            Db.Accounts.Add(account);
            return account;
        }
        public Account LogInVerification(string login, string password)
        {
            Account account = Db.Accounts.Find(i => i.Login == login);
            if(account == null || account.Password != password)
            {
                throw new ArgumentException("Invalid login or password!");
            }
            return account;
        }
    }
}

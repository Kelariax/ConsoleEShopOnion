﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using ConsoleEShopOnion.Infrastacture.Data.Context;
using ConsoleEShopOnion.Infrastacture.Data.Repository;
using System;
using System.Collections.Generic;


namespace ConsoleEShopOnion.Infrastacture.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext db = new DbContext();

       
        private AccountsRepository _accountsRepository;
        private GoodsRepository _goodsRepository;
        private OrdersRepository _ordersRepository;

        private UnitOfWork ()
        {
        }

        private static UnitOfWork instance;
        public static UnitOfWork GetInstance()
        {
            if(instance == null)
            {
                instance = new UnitOfWork();
            }
            return instance;
        }

        public IRepository<Account> Accounts
        {
            get
            {
                if(_accountsRepository == null)
                {
                    _accountsRepository = new AccountsRepository(db);
                }
                return _accountsRepository;
            }
        }
        public IRepository<Good> Goods
        {
            get
            {
                if(_goodsRepository == null)
                {
                    _goodsRepository = new GoodsRepository(db);
                }
                return _goodsRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if(_ordersRepository == null)
                {
                    _ordersRepository = new OrdersRepository(db);
                }
                return _ordersRepository;
            }
        }
    }
}

﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShopOnion
{
    abstract class RegisteredActions : UserActions
    {

        public EventHandler LogOutNotify { get; set; }
        protected RegisteredActions(IUnitOfWork db, Account account)
            : base(db, account)
        {
            Functions.Add(3, this.CreateOrder);
            Functions.Add(4, this.ConfirmOrder);
            Functions.Add(9, this.LogOut);
        }

        public void CreateOrder()
        {
            Order order = new Order(Account.Id);
            Good good = null;
            do
            {
                Console.WriteLine("<<Create order>>");
                Console.WriteLine(order);
                Console.Write("\nEnter good name: ");
                string name = Console.ReadLine();
                try
                {
                    good = GoodsService.GetGoodByName(name);
                    int quantity;
                    Console.Write("Enter quantity of good: ");
                    quantity = int.Parse(Console.ReadLine());

                    OrdersService.AddGoodToOrder(order, new OrderItem(good, quantity));

                    Console.Clear();
                    Console.WriteLine("<<Create order>>");
                    Console.WriteLine(order);
                    Console.WriteLine("\nGood was added to the order");
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    if (order.OrderItems.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("<<Create order>>");
                        Console.WriteLine(order);

                        Console.WriteLine("\nOrder successfully created");
                        Console.WriteLine("\nPress any key...");
                        OrdersService.CreateOrder(order, Account.OrdersId);
                        Console.ReadKey();
                    }
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        public void ConfirmOrder()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order confirming>>");
                Console.Write("Enter order id: ");
                try
                {
                    id = int.Parse(Console.ReadLine());
                    OrdersService.ConfirmOrder(id, Account.OrdersId);
                    Console.WriteLine("Order successfully confirmed!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);                    
                }
                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);
        }

        protected void EditPersonalInfo(Account account)
        {
            int command;
            do
            {
                Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                Console.WriteLine(account.personInfo);
                Console.WriteLine("\n1 - Name\n2 - Surname\n3 - Phone\n4 - Email\n5 - Back");
                Console.Write("Choose command: ");
                try
                {
                    command = int.Parse(Console.ReadLine());
                    if(command == 5)
                    {
                        break;
                    }
                    Console.WriteLine("Enter value: ");
                    AccountService.EditPersonalInfo(account.personInfo, Console.ReadLine(), command);
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);

                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                }
                Console.Clear();
            } while (true);
        }

        public void LogOut()
        {
            LogOutNotify?.Invoke(null, new EventArgs());
        }
    }
}

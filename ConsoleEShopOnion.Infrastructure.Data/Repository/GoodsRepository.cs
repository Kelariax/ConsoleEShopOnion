﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using ConsoleEShopOnion.Infrastacture.Data.Context;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleEShopOnion.Infrastacture.Data.Repository
{
    public class GoodsRepository : IRepository<Good>, IEnumerable<Good>
    {
        private readonly DbContext db;
        public GoodsRepository(DbContext db)
        {
            this.db = db;
        }
        public Good this[int index]
        {
            get
            {
                if(index >= 0 && index < db.Goods.Count)
                {
                    return db.Goods[index];
                }

                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }
        public IEnumerable<Good> GetAll()
        {
            return db.Goods;
        }
        public void Add(Good item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(new string("The good is null"));
            }
            if(db.Goods.Contains(item))
            {
                throw new ArgumentException("That item already exists in collection", nameof(item));
            }
            db.Goods.Add(item);
        }       

        public Good Find(Predicate<Good> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Goods.Find(predicate);
        }

        public int IndexOf(Predicate<Good> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Goods.FindIndex(predicate);
        }
        public bool Contains(Good item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(new string("The good is null"));
            }
            foreach (Good good in db.Goods)
            {
                if (good.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Contains(Predicate<Good> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentException("Predicate is null");
            }
            return db.Goods.Exists(predicate);
        }
        public IEnumerator<Good> GetEnumerator()
        {
            return db.Goods.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return db.Goods.GetEnumerator();
        }
    }
}

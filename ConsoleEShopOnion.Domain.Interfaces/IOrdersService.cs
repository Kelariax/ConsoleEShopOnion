﻿using ConsoleEShopOnion.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Interfaces
{
    public interface IOrdersService
    {
        IUnitOfWork Db { get; set; }
        IEnumerable<Order> GetAllOrders();
        IEnumerable<Order> GetAllAccountOrders(int accountId);
        Order GetOrderById(int id);
        void AddGoodToOrder(Order order, OrderItem orderItem);
        void ConfirmOrder(int id, List<int> ordersId);
        void CancelOrderByUser(int id, List<int> ordersId);
        void CancelOrderByAdmin(int id);
        void CreateOrder(Order order, List<int> ordersId);
        void ChangeOrderStatus(int id, OrderStatus newOrderStatus);
        void SetOrderStatusRecieved(int id, List<int> ordersId);
        OrderStatus SelectOrderStatus(int selectedStatus);

    }
}

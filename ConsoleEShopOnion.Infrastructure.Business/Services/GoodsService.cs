﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Infrastructure.Business.Services
{
    public class GoodsService : IGoodsService
    {
        public IUnitOfWork Db { get; set; }
        public GoodsService(IUnitOfWork db)
        {
            this.Db = db;
        }
        public IEnumerable<Good> GetAllGoods()
        {
            return Db.Goods.GetAll();
        }       
        public void AddNewGood(Good good)
        {
            if (Db.Goods.Contains(good))
            {
                throw new ArgumentException($"Good with name '{good.Name}' already exists!");
            }
            if (good.Name == "Empty" || good.Category == "Empty" || good.Price == 0)
            {
                throw new ArgumentException("Fields name, category, price can't be empty!");
            }
            Db.Goods.Add(good);
        }
        public Good GetGoodByName(string name)
        {
            Good good = Db.Goods.Find(i => i.Name == name);
            if(good == null)
            {
                throw new ArgumentException($"There is no good with name '{name}'!");
            }
            return good;
        }
        public void EditGoodInfo(Good good, string value, int selectedField)
        {
            switch (selectedField)
            {
                case 1:
                    good.Name = value;
                    break;
                case 2:
                    good.Category = value;
                    break;
                case 3:
                    good.Description = value;
                    break;
                case 4:
                    good.Price = decimal.Parse(value);
                    break;
                default:
                    throw new ArgumentException("There is no such field!");
            }
        }
    }
}

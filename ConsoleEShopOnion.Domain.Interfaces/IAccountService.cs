﻿using ConsoleEShopOnion.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Interfaces
{
    public interface IAccountService
    {
        IUnitOfWork Db { get; set;}

        IEnumerable<Account> GetAllAccounts();
        void EditPersonalInfo(PersonInfo personInfo, string value, int selectedField);
        Account GetAccountByLogin(string login);        

        Account RegisterAccount(string login, string password);

        Account LogInVerification(string login, string password);


        
    }
}

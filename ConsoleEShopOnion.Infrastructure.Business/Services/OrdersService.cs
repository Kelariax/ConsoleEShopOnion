﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ConsoleEShopOnion.Domain.Interfaces;
using ConsoleEShopOnion.Domain.Core.Entities;

namespace ConsoleEShopOnion.Infrastructure.Business.Services
{
    public class OrdersService : IOrdersService
    {
        public IUnitOfWork Db { get; set; }
        public OrdersService(IUnitOfWork db)
        {
            this.Db = db;
        }
        public IEnumerable<Order> GetAllOrders()
        {
            return Db.Orders.GetAll();
        }
        public IEnumerable<Order> GetAllAccountOrders(int accountId)
        {
            var orders = Db.Orders.GetAll().Where(o => o.UserId == accountId);
            if(orders == null)
            {
                throw new ArgumentException("You don't have any orders!");
            }
            return orders;
        }
        public Order GetOrderById(int id)
        {
            var order = Db.Orders.Find(i => i.Id == id);
            if (order == null)
            {
                throw new ArgumentException("There is no order with that id!");
            }
            return order;
        }
        public void AddGoodToOrder(Order order, OrderItem orderItem)
        {
            order.Add(orderItem);
        }
        public void CreateOrder(Order order, List<int> ordersId)
        {
            if (!order.OrderItems.Any())
            {
                throw new ArgumentException("There are no goods in order!");
            }
            Db.Orders.Add(order);
            ordersId.Add(order.Id);
        }
        public void ConfirmOrder(int id, List<int> ordersId)
        {
            Order order = Db.Orders.Find(i => i.Id == id);            
            if (order == null || !ordersId.Contains(order.Id))
            {
                throw new ArgumentException($"You don't have order with that id!");
            }
            if(order.IsConfirmed)
            {
                throw new ArgumentException($"Order '{order.Id}' already confirmed!");
            }
            if(order.Status != OrderStatus.New)
            {
                throw new ArgumentException("You can't confirm canceled order!");
            }
            order.IsConfirmed = true;
        }
        public void CancelOrderByUser(int id, List<int> ordersId)
        {
            Order order = Db.Orders.Find(i => i.Id == id);
            if (order == null || !ordersId.Contains(order.Id))
            {
                throw new ArgumentException($"You don't have order with that id !");
            }
            CancelOrder(order, PermisionStatus.RegisteredUser);
        }
        public void CancelOrderByAdmin(int id)
        {
            Order order = Db.Orders.Find(i => i.Id == id);
            CancelOrder(order, PermisionStatus.Admin);
        }
        private void CancelOrder(Order order, PermisionStatus permision)
        {
            if (order == null )
            {
                throw new ArgumentException($"There is no order with that id !");
            }
            if(order.Status == OrderStatus.Completed)
            {
                throw new ArgumentException("You can't cancel completed order");
            }
            if(order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser)
            {
                throw new ArgumentException("That order already canceled!");
            }
            if(order.Status == OrderStatus.Received)
            {
                throw new ArgumentException("You can't cancel order that already received!");
            }
            switch (permision)
            {
                case PermisionStatus.RegisteredUser:
                            order.Status = OrderStatus.CanceledByUser;
                    break;
                case PermisionStatus.Admin:
                            order.Status = OrderStatus.CanceledByAdmin;
                    break;
            }
        }
        public void SetOrderStatusRecieved(int id, List<int> ordersId)
        {
            Order order = Db.Orders.Find(i => i.Id == id);
            if (order == null || !ordersId.Contains(order.Id))
            {
                throw new ArgumentException($"You don't have order with that id !");
            }
            if (order.Status == OrderStatus.Completed)
            {
                throw new ArgumentException("You can't receive completed order");
            }
            if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser)
            {
                throw new ArgumentException("You can't receive canceled order!");
            }
            if(order.Status != OrderStatus.Sent)
            {
                throw new ArgumentException("You can't receive order that wasn't sent");
            }
            order.Status = OrderStatus.Received;
        }
        public void ChangeOrderStatus(int id, OrderStatus newOrderStatus)
        {
            Order order = Db.Orders.Find(i => i.Id == id);
            if(order == null)
            {
                throw new ArgumentException("There no order with that id!");
            }
            if(!order.IsConfirmed && newOrderStatus != OrderStatus.CanceledByAdmin || newOrderStatus != OrderStatus.CanceledByUser)
            {
                throw new ArgumentException("You can't change not confirmed order status");
            }
            if(order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser)
            {
                throw new ArgumentException("You can't change canceled order status!");
            }
            if (order.Status == OrderStatus.Completed)
            {
                throw new ArgumentException("You can't change completed order status!");
            }
            order.Status = newOrderStatus;
        }

        public OrderStatus SelectOrderStatus(int selectedStatus)
        {
            switch (selectedStatus)
            {
                case 1:
                    return OrderStatus.CanceledByAdmin;
                   
                case 2:
                    return OrderStatus.Sent;
                    
                case 3:
                    return OrderStatus.ReceivedPayment;
                case 4:
                    return OrderStatus.Completed;                    
                default:
                    throw new ArgumentException("There is no such status!");
            }
        }
    }
}

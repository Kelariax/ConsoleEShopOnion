﻿using ConsoleEShopOnion.Infrastacture.Data.UoW;
using System;
using System.Collections.Generic;

namespace ConsoleEShopOnion
{
    class Program
    {
        static void Main(string[] args)
        {
            EShopController eShopController = new EShopController(UnitOfWork.GetInstance());
            eShopController.Run();
        }
    }
}
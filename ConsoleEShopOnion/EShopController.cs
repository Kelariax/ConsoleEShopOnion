﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Infrastacture.Data.UoW;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion
{

    public class EShopController
    {
        public Account account;
        public UnitOfWork db;
        bool exit = false;

        public EShopController(UnitOfWork db)
        {
            this.db = db;
        }
        public void Run()
        {
            account = null;
            UserActions userMenu = null;
            
            int command;
            while (!exit)
            {
                command = 0;
                try
                {
                    if (account == null)
                    {
                        userMenu = new GuestActions(db, account);
                        userMenu.ExitNotify += ExitHandler;
                        ((GuestActions)userMenu).LogInNotify += LogInHandler;
                        ((GuestActions)userMenu).RegisterUserNotify += RegisterUserHandler;
                        userMenu.OutputUserFunctions();

                        Console.Write("Choose command: ");

                        command = Convert.ToInt32(Console.ReadLine());

                        Console.Clear();

                        userMenu.ExecuteCommand(command);
                    }
                    else
                    {
                        
                        switch (account.Permision)
                        {
                            case PermisionStatus.RegisteredUser:

                                userMenu = new ClientActions(db, account);
                                ((RegisteredActions)userMenu).LogOutNotify += LogOutHandler;
                                userMenu.ExitNotify += ExitHandler;
                                ((ClientActions)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);

                                break;
                            case PermisionStatus.Admin:
                                userMenu = new AdminActions(db, account);
                                ((RegisteredActions)userMenu).LogOutNotify += LogOutHandler;
                                userMenu.ExitNotify += ExitHandler;
                                ((AdminActions)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);
                                break;
                        }
                    }


                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!userMenu.ContinueCheck())
                    {
                        break;
                    }
                }
                Console.Clear();
            }
        }

        public void LogInHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        }

        public void RegisterUserHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        }

        public void LogOutHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        } 
        public void ExitHandler(object sender, EventArgs e)
        {
            exit = Convert.ToBoolean(sender);
        }
    }
}

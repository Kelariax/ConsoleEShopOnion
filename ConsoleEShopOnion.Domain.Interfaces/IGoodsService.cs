﻿using ConsoleEShopOnion.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Interfaces
{
    public interface IGoodsService
    {
        IUnitOfWork Db { get; set; }
        IEnumerable<Good> GetAllGoods();
        Good GetGoodByName(string name);
        void AddNewGood(Good good);
        public void EditGoodInfo(Good good, string value, int selectedField);
    }
}

﻿using ConsoleEShopOnion.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Infrastacture.Data.Context
{
    public class DbContext
    {
        public  List<Account> Accounts { get; set; } = InitialData.Accounts;
        public  List<Good> Goods { get; set; } = InitialData.Goods;
        public  List<Order> Orders { get; set; } = InitialData.Orders;
    }
}

﻿using ConsoleEShopOnion.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
namespace ConsoleEShopOnion.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        public IRepository<Account> Accounts { get;}
        public IRepository<Good> Goods { get;}
        public IRepository<Order> Orders { get;}

    }
}

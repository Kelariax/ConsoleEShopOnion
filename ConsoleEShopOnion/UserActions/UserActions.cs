﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Interfaces;
using ConsoleEShopOnion.Infrastructure.Business.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion
{
    public abstract class UserActions
    {
        public EventHandler ExitNotify { get; set; }
        public Account Account { get; set; }
        public Dictionary<int, Action> Functions { get; set; }
        
        protected IAccountService AccountService { get; }
        protected IGoodsService GoodsService { get; }
        protected IOrdersService OrdersService { get; }



        protected UserActions (IUnitOfWork db)
        {
            Functions = new Dictionary<int, Action> {
                {1, this.OutputGoods },
                {2, this.FindGood },
                {0, this.Exit }
            };
            AccountService = new AccountService(db);
            GoodsService = new GoodsService(db);
            OrdersService = new OrdersService(db);
        }
        protected UserActions(IUnitOfWork db, Account account)
            :this(db)
        {
            this.Account = account;
        }
        public abstract void OutputUserFunctions();
        public void OutputGoods()
        {
            int count = 0;
            int page = 1;
            Console.WriteLine($"<<Goods list>><<Page: {page}>>");
            foreach (var item in GoodsService.GetAllGoods())
            {
                Console.WriteLine(item + "\n");
                count++;
                if (count % 5 == 0)
                {
                    Console.WriteLine("Any key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        break;
                    }
                    Console.Clear();
                    page++;
                    Console.WriteLine($"<<Goods list>><<Page: {page}>>");
                }
            }
            Console.WriteLine("\nPress any key...");
            Console.ReadKey();
        }
        public void FindGood()
        {
            Good good;
            do
            {
                Console.WriteLine("<<Good search>>");
                Console.Write("Enter good name: ");
                try
                {
                    good = GoodsService.GetGoodByName(Console.ReadLine());
                    Console.Clear();
                    Console.WriteLine(good);

                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);    
        }
        public bool ContinueCheck()
        {
            var symb = Console.ReadKey();
            if (symb.Key == ConsoleKey.Escape)
            {
                return false;
            }
            return true;
        }

        public void ExecuteCommand(int command)
        {
            if(Functions.ContainsKey(command))
            {
                Functions[command].Invoke();
            }
            else
            {
                Console.WriteLine("\nThere is no such command");
                Console.WriteLine("\nPress any key...");
                Console.ReadKey();
            }
        }
        public void Exit()
        {
            ExitNotify?.Invoke(true, new EventArgs());
        }
    }
}

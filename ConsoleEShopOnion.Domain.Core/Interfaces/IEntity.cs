﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Core.Interfaces
{
    public interface IEntity
    {
         int Id { get; }
    }
}

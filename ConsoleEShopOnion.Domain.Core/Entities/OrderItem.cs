﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Core.Entities
{ 
    public class OrderItem
    {
        public Good Good { get; set; }
        public int GoodQuantity { get; set; }

        public OrderItem(Good good, int goodQuantity)
        {
            Good = good;
            GoodQuantity = goodQuantity;
        }

        public override string ToString()
        {
            return new string($"Name: {Good.Name} || Price: {Good.Price} || Quantity: {GoodQuantity}");
        }
    }
}

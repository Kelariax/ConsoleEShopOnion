﻿using ConsoleEShopOnion.Domain.Core.Entities;
using ConsoleEShopOnion.Domain.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShopOnion.Domain.Interfaces
{
    public interface IRepository<T> where T: IEntity
    {
        IEnumerable<T> GetAll();
        void Add(T item);

        T Find(Predicate<T> predicate);

        int IndexOf(Predicate<T> predicate);

        bool Contains(T item);

        bool Contains(Predicate<T> predicate);
    }
}
